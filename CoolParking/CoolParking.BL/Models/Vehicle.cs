﻿// TODO: implement class Vehicle.
//       Properties: Id (string), VehicleType (VehicleType), Balance (decimal).
//       The format of the identifier is explained in the description of the home task.
//       Id and VehicleType should not be able for changing.
//       The Balance should be able to change only in the CoolParking.BL project.
//       The type of constructor is shown in the tests and the constructor should have a validation, which also is clear from the tests.
//       Static method GenerateRandomRegistrationPlateNumber should return a randomly generated unique identifier.
using System;

using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

public class Vehicle
{
    [Required (ErrorMessage = "Номер авто")]
    [RegularExpression(@"^\d{2}-\2{4}-\d{2}$", ErrorMessage = "Номер авто должен быть формата ХХ-YYYY-XX")]
    public string Id { get; set; }
    public VehicleType vehicleType { get; set; }
    [Required]
    [Range(0,1000000, ErrorMessage = "Недопустимый баланс")]
    public decimal Balance { get; set; }

   


    public Vehicle(string Id, VehicleType vehicleType, decimal Balance)
    {

        this.Id = Id;
        this.vehicleType = vehicleType;
        this.Balance = Balance;
    }


}