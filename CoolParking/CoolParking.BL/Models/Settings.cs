﻿public static class Setting
{
    public static decimal BalanceParking = 0;
    public static int SizeParking = 10;
    public static int PaymentPeriod = 5;
    public static int EntriLog = 60;
    public static double PenaltyRate = 2.5;
    public static double PassengerCarRate = 2;
    public static double TruckRate = 5;
    public static double BusRate = 3.5;
    public static double MotorcycleRate = 1;
}