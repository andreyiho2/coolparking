﻿// TODO: implement the ParkingService class from the IParkingService interface.
//       For try to add a vehicle on full parking InvalidOperationException should be thrown.
//       For try to remove vehicle with a negative balance (debt) InvalidOperationException should be thrown.
//       Other validation rules and constructor format went from tests.
//       Other implementation details are up to you, they just have to match the interface requirements
//       and tests, for example, in ParkingServiceTests you can find the necessary constructor format and validation rules.

using CoolParking.BL.Interfaces;
using System.Collections.ObjectModel;
using System.Linq;

public class ParkingService : IParkingService
{
    public decimal GetBalance()
    {
        return Setting.BalanceParking;
    }
    public int GetCapacity()
    {
        return 0;
    }
    public int GetFreePlaces()
    {
        return Setting.SizeParking - Parking.auto.Count;
    }

    public ReadOnlyCollection<Vehicle> readOnlyVehicle =
            new ReadOnlyCollection<Vehicle>(Parking.auto);

    public ReadOnlyCollection<Vehicle> GetVehicles()
    {
        return new ReadOnlyCollection<Vehicle>(Parking.auto);
    }
    public void AddVehicle(Vehicle vehicle)
    {
       
        
        Parking.auto.Add(vehicle);
    }
    public void RemoveVehicle(string vehicleId)
    {
        var itemToRemove = Parking.auto.SingleOrDefault(r => r.Id == vehicleId);
        if (itemToRemove != null)
            Parking.auto.Remove(itemToRemove);
    }
    public void TopUpVehicle(string vehicleId, decimal sum)
    {
        var itemToRemove = Parking.auto.SingleOrDefault(r => r.Id == vehicleId);
        if (itemToRemove != null)
            itemToRemove.Balance += sum;
    }





    //public TransactionInfo[] GetLastParkingTransactions()
    //{
    //    throw new System.NotImplementedException();
    //}

    public string ReadFromLog()
    {
        throw new System.NotImplementedException();
    }

    public void Dispose()
    {
        throw new System.NotImplementedException();
    }
}