﻿using CoolParking.BL.Interfaces;
using System;

namespace CoolParking.BL
{
    class Program
    {
        static void Main(string[] args)
        {
            int option = 1;
            ParkingService _parkingService = new();
            
            
            while (option != 0)
            {
                Console.WriteLine("1 - Вывести на экран текущий баланс Парковки.\n" +
                "2 - Вывести на экран сумму заработанных денег за текущий период(до записи в лог).\n" +
                "3 - Вывести на экран количество свободных / занятых мест на парковке.\n" +
                "4 - Вывести на экран все Транзакции Парковки за текущий период(до записи в лог).\n" +
                "5 - Вывести историю транзакций(считав данные из файла Transactions.log).\n" +
                "6 - Вывести на экран список Тр.средств находящихся на Паркинге.\n" +
                "7 - Поставить Тр.средство на Паркинг.\n" +
                "8 - Забрать транспортное средство с Паркинга.\n" +
                "9 - Пополнить баланс конкретного Тр.средства.\n");
                option = Convert.ToInt32(Console.ReadLine());
                switch (option)
                {
                    case 1:
                        Console.WriteLine($"Баланс Парковки: {_parkingService.GetBalance()}");
                        break;
                    case 3:
                        Console.WriteLine($"Свободних мест: {_parkingService.GetFreePlaces()}");
                        break;
                    case 6:
                        Console.WriteLine();
                        for (var index = 0; index < _parkingService.readOnlyVehicle.Count; index++)
                        {
                            Console.Write(_parkingService.readOnlyVehicle[index].Id + " "
                                +_parkingService.readOnlyVehicle[index].vehicleType + " "
                                +_parkingService.readOnlyVehicle[index].Balance + "\n");
                }

                break;
                    case 7:
                        Console.Write("Номер авто: ");
                        string Id = Convert.ToString(Console.ReadLine());

                         Vehicle vehicle = new(Id, VehicleType.Bus, 10);
                        _parkingService.AddVehicle(vehicle);
                        break;



                }
            }
        }
    }
}
